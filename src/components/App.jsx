import React from 'react';
import Comment from './comment';

class App extends React.Component {
    state = {
        names: ["Alex", "Sam", "Jane"],
    }
    render() {
        return (
            <React.Fragment>
                <div className="container">
                    {this.state.names.map(item => <Comment name={item} key={Date.now()} />)}
                </div>
            </React.Fragment>
        )
    }
}

export default App;
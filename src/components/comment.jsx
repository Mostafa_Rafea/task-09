import React from 'react';

class Comment extends React.Component {
    state = {
        currentDate: new Date(),
        counter: 0
    };

    increment = () => {
        let counter = this.state.counter;
        counter++;
        this.setState({ counter });
    }
    render() {

        return (
            <div className="card mb-3" style={{ 'max-width': 400 }}>
                <div className="row no-gutters">
                    <div className="col-md-4 d-flex align-items-center flex-column py-3">
                        <i className="fas fa-portrait fa-5x d-block" />
                        <i className="fas fa-thumbs-up fa-2x mt-3" onClick={this.increment} />
                    </div>
                    <div className="col-md-8">
                        <div className="card-body d-flex align-items-center">
                            <section className="w-100">
                                <section className="d-flex justify-content-between">
                                    <h5 className="card-title">{this.props.name}</h5>
                                    <p className="card-text text-muted">Today at {this.state.currentDate.getHours()}:{this.state.currentDate.getMinutes()}</p>
                                </section>
                                <p className="card-text">Great Blog Post!!</p>
                                <p className="mb-0 pt-4">{this.state.counter}</p>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Comment;